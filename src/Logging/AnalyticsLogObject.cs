using System;
using System.Collections.Generic;

namespace Coscine.Logging
{
    [Serializable]
    public class AnalyticsLogObject
    {
        private string _type = "";
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (value == null)
                {
                    _type = "";
                }
                else
                {
                    _type = value;
                }
            }
        }

        private string _operation = "";

        public string Operation
        {
            get
            {
                return _operation;
            }
            set
            {
                if (value == null)
                {
                    _operation = "";
                }
                else
                {
                    _operation = value;
                }
            }
        }

        // Default is the current time
        private string _timestamp = FormatTimestamp(DateTime.UtcNow);

        public string Timestamp
        {
            get
            {
                return _timestamp;
            }
            set
            {
                if (value == null)
                {
                    _timestamp = FormatTimestamp(DateTime.UtcNow);
                }
                else
                {
                    _timestamp = value;
                }
            }
        }

        private string _userId = "";

        public string UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                if (value == null)
                {
                    _userId = "";
                }
                else
                {
                    _userId = value;
                }
            }
        }

        private string _roleId = "";

        public string RoleId
        {
            get
            {
                return _roleId;
            }
            set
            {
                if (value == null)
                {
                    _roleId = "";
                }
                else
                {
                    _roleId = value;
                }
            }
        }

        private string _sessionId = "";

        public string SessionId
        {
            get
            {
                return _sessionId;
            }
            set
            {
                if (value == null)
                {
                    _sessionId = "";
                }
                else
                {
                    _sessionId = value;
                }
            }
        }

        private string _clientCorrolationId = "";

        public string ClientCorrolationId
        {
            get
            {
                return _clientCorrolationId;
            }
            set
            {
                if (value == null)
                {
                    _clientCorrolationId = "";
                }
                else
                {
                    _clientCorrolationId = value;
                }
            }
        }

        private string _projectId = "";

        public string ProjectId
        {
            get
            {
                return _projectId;
            }
            set
            {
                if (value == null)
                {
                    _projectId = "";
                }
                else
                {
                    _projectId = value;
                }
            }
        }

        private List<string> _quotaSize = new List<string>();

        public List<string> QuotaSize
        {
            get
            {
                return _quotaSize;
            }
            set
            {
                if (value == null)
                {
                    _quotaSize = new List<string>();
                }
                else
                {
                    _quotaSize = value;
                }
            }
        }

        private string _resourceId = "";

        public string ResourceId
        {
            get
            {
                return _resourceId;
            }
            set
            {
                if (value == null)
                {
                    _resourceId = "";
                }
                else
                {
                    _resourceId = value;
                }
            }
        }

        private string _fileId = "";

        public string FileId
        {
            get
            {
                return _fileId;
            }
            set
            {
                if (value == null)
                {
                    _fileId = "";
                }
                else
                {
                    _fileId = value;
                }
            }
        }

        private string _applicationsProfile = "";

        public string ApplicationsProfile
        {
            get
            {
                return _applicationsProfile;
            }
            set
            {
                if (value == null)
                {
                    _applicationsProfile = "";
                }
                else
                {
                    _applicationsProfile = value;
                }
            }
        }

        private string _metadataCompleteness = "";

        public string MetadataCompleteness
        {
            get
            {
                return _metadataCompleteness;
            }
            set
            {
                if (value == null)
                {
                    _metadataCompleteness = "";
                }
                else
                {
                    _metadataCompleteness = value;
                }
            }
        }

        private string _license = "";

        public string License
        {
            get
            {
                return _license;
            }
            set
            {
                if (value == null)
                {
                    _license = "";
                }
                else
                {
                    _license = value;
                }
            }
        }

        private List<string> _disciplines = new List<string>();

        public List<string> Disciplines
        {
            get
            {
                return _disciplines;
            }
            set
            {
                if (value == null)
                {
                    _disciplines = new List<string>();
                }
                else
                {
                    _disciplines = value;
                }
            }
        }

        private List<string> _organizations = new List<string>();

        public List<string> Organizations
        {
            get
            {
                return _organizations;
            }
            set
            {
                if (value == null)
                {
                    _organizations = new List<string>();
                }
                else
                {
                    _organizations = value;
                }
            }
        }

        private string _visibility = "";

        public string Visibility
        {
            get
            {
                return _visibility;
            }
            set
            {
                if (value == null)
                {
                    _visibility = "";
                }
                else
                {
                    _visibility = value;
                }
            }
        }

        private List<string> _projectList = new List<string>();

        public List<string> ProjectList
        {
            get
            {
                return _projectList;
            }
            set
            {
                if (value == null)
                {
                    _projectList = new List<string>();
                }
                else
                {
                    _projectList = value;
                }
            }
        }

        private List<string> _resourceList = new List<string>();

        public List<string> ResourceList
        {
            get
            {
                return _resourceList;
            }
            set
            {
                if (value == null)
                {
                    _resourceList = new List<string>();
                }
                else
                {
                    _resourceList = value;
                }
            }
        }

        private List<string> _userList = new List<string>();

        public List<string> UserList
        {
            get
            {
                return _userList;
            }
            set
            {
                if (value == null)
                {
                    _userList = new List<string>();
                }
                else
                {
                    _userList = value;
                }
            }
        }

        private string _externalInfo = "";

        public string ExternalInfo
        {
            get
            {
                return _externalInfo;
            }
            set
            {
                if (value == null)
                {
                    _externalInfo = "";
                }
                else
                {
                    _externalInfo = value;
                }
            }
        }

        public AnalyticsLogObject(string type, string operation, DateTime timestamp, string userId, string roleId, string sessionId, string clientCorrolationId, string projectId, List<string> quotaSize, string resourceId, string fileId, string applicationsProfile, string metadataCompleteness, string license, List<string> disciplines, List<string> organizations, string visibility, List<string> projectList, List<string> resourceList, List<string> userList, string externalInfo)
        {
            Type = type;
            Operation = operation;
            Timestamp = FormatTimestamp(timestamp);
            UserId = userId;
            RoleId = roleId;
            SessionId = sessionId;
            ClientCorrolationId = clientCorrolationId;
            ProjectId = projectId;
            QuotaSize = quotaSize;
            ResourceId = resourceId;
            FileId = fileId;
            ApplicationsProfile = applicationsProfile;
            MetadataCompleteness = metadataCompleteness;
            License = license;
            Disciplines = disciplines;
            Organizations = organizations;
            Visibility = visibility;
            ProjectList = projectList;
            ResourceList = resourceList;
            UserList = userList;
            ExternalInfo = externalInfo;
        }

        public AnalyticsLogObject()
        {
        }

        private static string FormatTimestamp(DateTime timestamp)
        {
            string formattedDateTime = string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}", timestamp);
            return formattedDateTime;
        }
    }
}
