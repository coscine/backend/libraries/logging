﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace Coscine.Logging
{
    [LayoutRenderer("CoscineLogLevel")]
    class LogLevelLayoutRenderer : LayoutRenderer
    {

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {

            string levelName;
            if (logEvent.Level == LogLevel.Info)
            {
                if (MappedDiagnosticsLogicalContext.Get("Analytics").Equals("true"))
                {
                    MappedDiagnosticsLogicalContext.Set("Analytics", false);
                    levelName = "Analytics";
                }
                else if (MappedDiagnosticsLogicalContext.Get("Reporting").Equals("true"))
                {
                    MappedDiagnosticsLogicalContext.Set("Reporting", false);
                    levelName = "Reporting";
                }
                else
                {
                    levelName = "Info";
                }

            }
            else if (logEvent.Level == LogLevel.Debug)
            {
                levelName = "Debug";
            }
            else if (logEvent.Level == LogLevel.Warn)
            {
                levelName = "Medium";
            }
            else if (logEvent.Level == LogLevel.Error)
            {
                levelName = "High";
            }
            else if (logEvent.Level == LogLevel.Fatal)
            {
                levelName = "Critical";
            }
            else
            {
                levelName = "Low";
            }
            builder.Append(levelName);
        }
    }
}
