﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog;
using System;

namespace Coscine.Logging
{

    public class CoscineLogger
    {
        private readonly Microsoft.Extensions.Logging.ILogger _logger;

        private const LogType defaultLogType = LogType.Low;

        public CoscineLogger(Microsoft.Extensions.Logging.ILogger logger)
        {
            CoscineLoggerConfiguration.UpdateActiveLogs();
            
            _logger = logger;
        }

        public void AnalyticsLog(AnalyticsLogObject analyticsLogObject)
        {
            analyticsLogObject.ClientCorrolationId = CoscineLoggerMetadata.GetClientCorrolationId();
            analyticsLogObject.UserId = CoscineLoggerMetadata.GetUserId();
            analyticsLogObject.SessionId = CoscineLoggerMetadata.GetSessionId();
            Log(LogType.Analytics, null, JsonConvert.SerializeObject(analyticsLogObject), null);
        }

        public void Log()
        {
            Log(defaultLogType, null, null, null);
        }

        public void Log(string message)
        {
            Log(defaultLogType, null, message, null);
        }

        public void Log(Exception exception)
        {
            Log(defaultLogType, null, null, exception);
        }

        public void Log(string message, Exception exception)
        {
            Log(defaultLogType, null, message, exception);
        }

        public void Log(Type context)
        {
            Log(defaultLogType, context, null, null);
        }

        public void Log(Type context, string message)
        {
            Log(defaultLogType, context, message, null);
        }

        public void Log(Type context, Exception exception)
        {
            Log(defaultLogType, context, null, exception);
        }

        public void Log(Type context, string message, Exception exception)
        {
            Log(defaultLogType, context, message, exception);
        }

        public void Log(LogType messageType)
        {
            Log(messageType, null, null, null);
        }

        public void Log(LogType messageType, Type context)
        {
            Log(messageType, context, null, null);
        }

        public void Log(LogType messageType, string message)
        {
            Log(messageType, null, message, null);
        }

        public void Log(LogType messageType, Exception exception)
        {
            Log(messageType, null, null, exception);
        }

        public void Log(LogType messageType, Type context, string message)
        {
            Log(messageType, context, message, null);
        }

        public void Log(LogType messageType, Type context, Exception exception)
        {
            Log(messageType, context, null, exception);
        }

        public void Log(LogType messageType, string message, Exception exception)
        {
            Log(messageType, null, message, exception);
        }

        public void Log(LogType messageType, Type context, string message, Exception exception)
        {
            CoscineLoggerConfiguration.UpdateActiveLogs();

            if (!CoscineLoggerConfiguration.IsLogLevelActivated(messageType))
            {
                return;
            }

            Microsoft.Extensions.Logging.LogLevel logLevel;

            switch (messageType)
            {
                case LogType.Debug:
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Debug;
                    break;
                case LogType.Low:
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
                    break;
                case LogType.Medium:
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Warning;
                    break;
                case LogType.High:
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Error;
                    break;
                case LogType.Critical:
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Critical;
                    break;
                case LogType.Analytics:
                    SetAnalytics(true);
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Information;
                    break;
                case LogType.Reporting:
                    SetReporting(true);
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Information;
                    break;
                default:
                    logLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
                    break;
            }

            _logger.Log(logLevel, exception, message);

        }

        private static void SetAnalytics(bool active)
        {
            var value = active ? "true" : "false";
            MappedDiagnosticsLogicalContext.Set("Analytics", value);
        }

        private static void SetReporting(bool active)
        {
            var value = active ? "true" : "false";
            MappedDiagnosticsLogicalContext.Set("Reporting", value);
        }
    }
}
