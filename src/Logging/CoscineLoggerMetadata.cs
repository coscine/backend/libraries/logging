﻿using NLog;
using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Coscine.Logging
{
    public class CoscineLoggerMetadata
    {
        public static void SetUserId(string userId = "00000000-0000-0000-0000-000000000000")
        {
            if (IsGuid(userId))
            {
                MappedDiagnosticsLogicalContext.Set("User", userId);
            }
            else
            {
                MappedDiagnosticsLogicalContext.Set("User", "");
            }
        }

        public static void SetUserId(Guid userId)
        {
            CoscineLoggerMetadata.SetUserId(userId.ToString());
        }

        public static string GetUserId()
        {
            return MappedDiagnosticsLogicalContext.Get("User");
        }

        public static void SetAnalytics(bool active)
        {
            var value = active ? "true" : "false";
            MappedDiagnosticsLogicalContext.Set("Analytics", value);
        }

        public static void SetReporting(bool active)
        {
            var value = active ? "true" : "false";
            MappedDiagnosticsLogicalContext.Set("Reporting", value);
        }

        public static void SetUri(string uri = "")
        {
            MappedDiagnosticsLogicalContext.Set("Uri", uri);
        }

        public static void SetCorrolationId()
        {
            Trace.CorrelationManager.ActivityId = Guid.NewGuid();
        }

        public static void SetClientCorrolationId(string clientCorrolationId = "00000000-0000-0000-0000-000000000000")
        {
            if (IsGuid(clientCorrolationId))
            {
                MappedDiagnosticsLogicalContext.Set("ClientCorrolationId", clientCorrolationId);
            }
            else
            {
                MappedDiagnosticsLogicalContext.Set("ClientCorrolationId", "00000000-0000-0000-0000-000000000000");
            }
        }

        public static void SetClientCorrolationId()
        {
            MappedDiagnosticsLogicalContext.Set("ClientCorrolationId", Guid.NewGuid());
        }

        public static void SetClientCorrolationId(Guid clientCorrolationId)
        {
            SetClientCorrolationId(clientCorrolationId.ToString());
        }

        public static string GetClientCorrolationId()
        {
            return MappedDiagnosticsLogicalContext.Get("ClientCorrolationId");
        }


        public static void SetSessionsId(string sessionId = "")
        {
            MappedDiagnosticsLogicalContext.Set("SessionId", sessionId);
        }

        public static string GetSessionId()
        {
            return MappedDiagnosticsLogicalContext.Get("SessionId");
        }

        public static void SetStatus(string status = "")
        {
            MappedDiagnosticsLogicalContext.Set("Status", status);
        }

        public static void SetClientTimeStamp(string clientTimeStamp = "")
        {
            MappedDiagnosticsLogicalContext.Set("ClientTimeStamp", clientTimeStamp);
        }

        private static bool IsGuid(string guid)
        {
            var match = Regex.Match(guid, @"^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$",
            RegexOptions.IgnoreCase);
            return match.Success;
        }
    }
}
