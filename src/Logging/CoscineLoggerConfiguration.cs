﻿using Consul;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using NLog.Targets;
using System;
using System.Text;
using LogLevel = NLog.LogLevel;

namespace Coscine.Logging
{
    public class CoscineLoggerConfiguration
    {
        // Keys for the database string
        private const string DbDataSourceKey = "coscine/global/db_data_source";
        private const string DbNameKey = "coscine/global/db_name";
        private const string DbUserIdKey = "coscine/global/db_user_id";
        private const string DbPasswordKey = "coscine/global/db_password";

        // path for the file logger
        private const string FileLoggerPath = "coscine/global/logging/fileloggerpath";

        // keys for the loglevels
        private const string LogLevelKeyDebug = "coscine/global/logging/levels/debug";
        private const string LogLevelKeyLow = "coscine/global/logging/levels/low";
        private const string LogLevelKeyMedium = "coscine/global/logging/levels/medium";
        private const string LogLevelKeyHigh = "coscine/global/logging/levels/high";
        private const string LogLevelKeyCritical = "coscine/global/logging/levels/critical";
        private const string LogLevelKeyAnalytics = "coscine/global/logging/levels/analytics";
        private const string LogLevelKeyReporting = "coscine/global/logging/levels/reporting";


        private static readonly bool[] activeLoglevels = new bool[Enum.GetNames(typeof(LogType)).Length];

        public static bool IsLogLevelActivated(LogType logType)
        {
            return activeLoglevels[(int)logType];
        }

        public static void SetConfig()
        {
            LayoutRenderer.Register<LogLevelLayoutRenderer>("CoscineLogLevel");

            var config = new LoggingConfiguration();


            var targetFileLogger = new FileTarget
            {
                Name = "fileTarget",
                FileName = GetStringFromConsul(FileLoggerPath),
                Layout = "${date:universalTime=true}|${mdlc:ClientTimeStamp}|${CoscineLogLevel}|${message}|${mdlc:User}|${mdlc:Uri}|${machinename}|${activityid}|${mdlc:Status}|${callsite}|${exception:format=message}${exception:format=stackTrace}"
            };


            var targetDatabase = new DatabaseTarget
            {
                Name = "database",
                ConnectionString = CoscineLoggerConfiguration.GetDbConnectionString(),
                CommandText = @"insert into dbo.Log ( [ServerTimeStamp], [ClientTimeStamp], [LogLevel], [Message], [Stacktrace], [UserId], [URI], [Server], [CorrolationId], [Status], [Source]) 
            values (
            CAST( @serverTimeStamp AS DATETIME2),
            CASE WHEN @clientTimeStamp = '' THEN NULL ELSE CAST( @clientTimeStamp AS DATETIME2) END, 
            @level,
            @message, 
            @stacktrace, 
            CASE WHEN @userId = '' THEN NULL ELSE convert(uniqueidentifier, @userId) END, 
            @uri, 
            @server, 
            CASE WHEN @corrolationId = '' THEN NULL ELSE convert(uniqueidentifier, @corrolationId) END, 
            @status, 
            @source)"

            };
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@serverTimeStamp", new NLog.Layouts.SimpleLayout("${date:universalTime=true}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@clientTimeStamp", new NLog.Layouts.SimpleLayout("${mdlc:ClientTimeStamp}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@level", new NLog.Layouts.SimpleLayout("${CoscineLogLevel}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@message", new NLog.Layouts.SimpleLayout("${message}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@stackTrace", new NLog.Layouts.SimpleLayout("${exception:format=message}${exception:format=stackTrace}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@userId", new NLog.Layouts.SimpleLayout("${mdlc:User}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@uri", new NLog.Layouts.SimpleLayout("${mdlc:Uri}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@server", new NLog.Layouts.SimpleLayout("${machinename}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@corrolationId", new NLog.Layouts.SimpleLayout("${activityid}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@status", new NLog.Layouts.SimpleLayout("${mdlc:Status}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@source", new NLog.Layouts.SimpleLayout("${callsite}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@type", new NLog.Layouts.SimpleLayout("${exception:format=type}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@innerException", new NLog.Layouts.SimpleLayout("${exception:format=:innerFormat=ShortType,Message,Method:MaxInnerExceptionLevel=1:InnerExceptionSeparator=}")));
            targetDatabase.Parameters.Add(new DatabaseParameterInfo("@additionalInfo", new NLog.Layouts.SimpleLayout("${message}")));

            var targetConsole = new NLog.Targets.ConsoleTarget
            {
                Name = "console",
                Layout = "${date:universalTime=true}|${mdlc:ClientTimeStamp}|${CoscineLogLevel}|${message}|${mdlc:User}|${mdlc:Uri}|${machinename}|${activityid}|${mdlc:Status}|${callsite}|${exception:format=message}${exception:format=stackTrace}"
            };


            config.AddTarget("fileLogger", targetFileLogger);
            config.AddTarget("databaseLogger", targetDatabase);
            config.AddTarget("consoleLogger", targetConsole);


            var blockAllDebug = new LoggingRule("blockAllDebug");
            blockAllDebug.SetLoggingLevels(LogLevel.Debug, LogLevel.Debug);
            blockAllDebug.LoggerNamePattern = "*";

            var blockAllTrace = new LoggingRule("blockAllTrace");
            blockAllTrace.SetLoggingLevels(LogLevel.Trace, LogLevel.Trace);
            blockAllTrace.LoggerNamePattern = "*";

            var blockAllWarning = new LoggingRule("blockAllWarning");
            blockAllWarning.SetLoggingLevels(LogLevel.Warn, LogLevel.Warn);
            blockAllWarning.LoggerNamePattern = "*";

            var blockAllError = new LoggingRule("blockAllError");
            blockAllError.SetLoggingLevels(LogLevel.Error, LogLevel.Error);
            blockAllError.LoggerNamePattern = "*";

            var blockAllFatal = new LoggingRule("blockAllFatal");
            blockAllFatal.SetLoggingLevels(LogLevel.Fatal, LogLevel.Fatal);
            blockAllFatal.LoggerNamePattern = "*";

            var blockAllInformation = new LoggingRule("blockAllInformation");
            blockAllInformation.SetLoggingLevels(LogLevel.Info, LogLevel.Info);
            blockAllInformation.LoggerNamePattern = "*";


            var ruleFileLogger = new LoggingRule("*", LogLevel.Error, LogLevel.Fatal, targetFileLogger);
            var ruleConsole = new LoggingRule("*", LogLevel.Error, LogLevel.Fatal, targetConsole);
            var ruleDatabase = new LoggingRule("*", LogLevel.Trace, LogLevel.Fatal, targetDatabase);

            config.LoggingRules.Add(blockAllDebug);
            config.LoggingRules.Add(blockAllTrace);
            config.LoggingRules.Add(blockAllWarning);
            config.LoggingRules.Add(blockAllError);
            config.LoggingRules.Add(blockAllFatal);
            config.LoggingRules.Add(blockAllInformation);

            config.LoggingRules.Add(ruleFileLogger);
            config.LoggingRules.Add(ruleConsole);
            config.LoggingRules.Add(ruleDatabase);

            LogManager.Configuration = config;

            UpdateActiveLogs();
        }


        public static void UpdateActiveLogs()
        {
            activeLoglevels[(int)LogType.Debug] = !GetStringFromConsul(LogLevelKeyDebug, "0").Equals("0");
            activeLoglevels[(int)LogType.Low] = !GetStringFromConsul(LogLevelKeyLow, "0").Equals("0");
            activeLoglevels[(int)LogType.Medium] = !GetStringFromConsul(LogLevelKeyMedium, "0").Equals("0");
            activeLoglevels[(int)LogType.High] = !GetStringFromConsul(LogLevelKeyHigh, "1").Equals("0");
            activeLoglevels[(int)LogType.Critical] = !GetStringFromConsul(LogLevelKeyCritical, "1").Equals("0");
            activeLoglevels[(int)LogType.Analytics] = !GetStringFromConsul(LogLevelKeyAnalytics, "0").Equals("0");
            activeLoglevels[(int)LogType.Reporting] = !GetStringFromConsul(LogLevelKeyReporting, "0").Equals("0");

            var config = LogManager.Configuration;
            config.FindRuleByName("blockAllInformation").Final = !activeLoglevels[(int)LogType.Analytics] && !activeLoglevels[(int)LogType.Reporting];
            config.FindRuleByName("blockAllDebug").Final = !activeLoglevels[(int)LogType.Debug];
            config.FindRuleByName("blockAllTrace").Final = !activeLoglevels[(int)LogType.Low];
            config.FindRuleByName("blockAllWarning").Final = !activeLoglevels[(int)LogType.Medium];
            config.FindRuleByName("blockAllError").Final = !activeLoglevels[(int)LogType.High];
            config.FindRuleByName("blockAllFatal").Final = !activeLoglevels[(int)LogType.Critical];

            LogManager.Configuration = config;
            LogManager.ReconfigExistingLoggers();
        }

        private static string GetDbConnectionString()
        {
            var dbDataSource = GetStringFromConsul(DbDataSourceKey);
            var dbDatabase = GetStringFromConsul(DbNameKey);
            var dbUserId = GetStringFromConsul(DbUserIdKey);
            var dbPassword = GetStringFromConsul(DbPasswordKey);
            return $"Data Source={dbDataSource}; Database={dbDatabase}; User Id={dbUserId}; Password={dbPassword};";
        }

        private static string GetStringFromConsul(string key)
        {
            using (var client = new ConsulClient())
            {
                var getPair = client.KV.Get(key);

                getPair.Wait();

                if (getPair.Result.Response?.Value != null)
                {
                    var value = getPair.Result.Response.Value;
                    return Encoding.UTF8.GetString(value, 0, value.Length);
                }
                else
                {
                    // TODO: Add logging that key has not been found
                    return null;
                }
            }
        }

        private static string GetStringFromConsul(string key, string defaultValue)
        {
            var value = GetStringFromConsul(key);
            if (value == null)
            {
                // TODO: Add logging that default value has been used
                return defaultValue;
            }
            return value;
        }
    }
}
