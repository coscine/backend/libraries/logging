﻿namespace Coscine.Logging
{

    public enum LogType
    {
        Debug = 0,
        Low = 1,
        Medium = 2,
        High = 3,
        Critical = 4,
        Analytics = 5,
        Reporting = 6
    }
}
